## tcp chat

both client.go and app.go contain main files, this is a command line
chat that uses a persistent tcp connection.

To use this run the app.go file in one terminal and client.go in another
terminal of the same computer(if you want to use different machines then
ip address has to be changed in both client and app).

`go run app.go` will run the app(chat server). `go run client.go` will
run the chat client.

#TODO:recover
