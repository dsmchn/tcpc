package main

import (
	"fmt"
	"log"
	"net"
	"net/http"
	_ "net/http/pprof"

	"bitbucket.org/dsmchn/tcpc/chatServer"
)

func main() {
	// chat handler
	// listen and serve at all ports
	l, err := net.Listen("tcp", "0.0.0.0:3333")
	if err != nil {
		log.Fatal(err)
	}
	defer l.Close()
	go func() {
		http.ListenAndServe("localhost:6060", nil)
	}()
	fmt.Println("listening on 0.0.0.0:3333")
	chat := chatServer.Chat{Connexions: make([]net.Conn, 0), Count: 0}
	for {
		conn := chat.Listen(l)
		go chat.Chat(conn)
	}

	// recover
	return
}
