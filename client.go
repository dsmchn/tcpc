package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"net"
	"os"

	"bitbucket.org/dsmchn/tcpc/chatServer"
	"github.com/satori/go.uuid"
)

func main() {
	conn, err := getTCPConn("localhost", "3333")
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()
	go MessageIn(conn)
	MessageOut(conn)
}

func getTCPConn(address string, port string) (net.Conn, error) {
	portAddr, err := net.ResolveTCPAddr("tcp", "localhost:3333")
	if err != nil {
		return nil, err
	}
	conn, err := net.DialTCP("tcp", nil, portAddr)
	if err != nil {
		return nil, err
	}
	return conn, nil

}
func MessageOut(conn net.Conn) {
	username, err := uuid.NewV4()
	if err != nil {
		log.Fatal(err)
	}
	reader := bufio.NewReader(os.Stdin)
	for {
		text, err := reader.ReadString('\n')
		if err != nil {
			log.Fatal(err)
		}
		err = chatServer.WriteMessage(conn, username.String()+"> "+text)
		if err != nil {
			log.Fatal(err)
		}
	}
}
func MessageIn(conn net.Conn) {
	for {
		msg, err := chatServer.ReadMessage(conn)
		if err == io.EOF {
			conn.Close()
			// server has closed connection, so client is
			//exit 0
			os.Exit(0)
		}
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println(msg)
	}

}
