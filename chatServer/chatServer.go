package chatServer

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"io"
	"log"
	"net"
	"os"
)

type Chat struct {
	Connexions []net.Conn
	Count      int
}

func (c *Chat) Listen(l net.Listener) net.Conn {
	conn, err := l.Accept()
	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}
	// set the connection from which message came as true
	c.AddConn(conn)
	return conn

}

// chat handler
func (c *Chat) Chat(conn net.Conn) {
	// chat handler broadcasts the message to channel
	// sets the current tcp connection to true
	for {
		msg, err := ReadMessage(conn)
		if err == io.EOF {
			c.RemoveConn(conn)
			conn.Close()
			return
		}
		if err != nil {
			log.Println(err)
			return
		}
		fmt.Printf("%s", msg)

		c.Broadcast(conn, msg)

	}
}

func (c *Chat) Broadcast(conn net.Conn, msg string) {
	for i := range c.Connexions {
		if c.Connexions[i] != conn {
			err := WriteMessage(c.Connexions[i], msg)
			if err != nil {
				log.Println(err)
			}
		}
	}
}

// write message
// the first 4bytes contain the length of the message
// the next bytes are written into connection
func WriteMessage(conn net.Conn, msg string) error {
	len, err := Int32ToBinary(int32(len([]byte(msg))))
	if err != nil {
		return err
	}
	_, err = conn.Write(len)
	if err != nil {
		return err
	}
	_, err = conn.Write([]byte(msg))
	if err != nil {
		fmt.Println(err)
		return err
	}
	return nil
}

// read string from connection
// the first 4 bytes hold the length of message
// for each of the next  len bits, we read one
// byte at a time and add this to the byte slice.
func ReadMessage(conn net.Conn) (string, error) {
	msgLength := make([]byte, 4)
	_, err := conn.Read(msgLength)
	if err != nil {
		return "", err
	}
	len, err := BinaryToInt32(msgLength)
	if err != nil {
		return "", err
	}
	reqLen := 0
	buff := make([]byte, int(len))
	for reqLen < int(len) {
		tempReqLen, err := conn.Read(buff[reqLen:])
		reqLen += tempReqLen
		if err != nil {
			return "", err
		}

	}
	return string(buff), nil
}

// function to convert int32 to binary
func Int32ToBinary(i int32) ([]byte, error) {
	buf := new(bytes.Buffer)
	err := binary.Write(buf, binary.BigEndian, i)
	return buf.Bytes(), err
}

// function to convert binary to int32
func BinaryToInt32(len []byte) (int32, error) {
	buf := bytes.NewReader(len)
	var result int32
	err := binary.Read(buf, binary.BigEndian, &result)
	return result, err
}

func (c *Chat) AddConn(conn net.Conn) {
	c.Connexions = append(c.Connexions, conn)
	c.Count = c.Count + 1
	fmt.Println("number of people in chat", c.Count)
	return
}

func (c *Chat) RemoveConn(conn net.Conn) {
	var i int
	for i = range c.Connexions {
		if c.Connexions[i] == conn {
			break
		}
	}
	c.Connexions = append(c.Connexions[:i], c.Connexions[i+1:]...)
	c.Count = c.Count - 1
	fmt.Println("number of people in chat", c.Count)
	return
}
