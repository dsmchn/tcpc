package main

import (
	"testing"

	"bitbucket.org/dsmchn/tcpc/chatServer"
)

func TestBinaryToInt(t *testing.T) {
	encoded, err := chatServer.Int32ToBinary(5)
	number, err := chatServer.BinaryToInt32(encoded)
	if err != nil {
		t.Errorf("got error: %v", err)
	}
	if number != 5 {
		t.Errorf("expected 5 Got :%v", number)
	}
}
